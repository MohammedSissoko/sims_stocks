package com.bein.sims_stocks.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="utilisateur")
public class Utilisateur implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long idutilisateur;
	private String nom;
	private String prenom;
	private String mail;
	private String mdp;
	private String photo;
	
	public Utilisateur() {
		super();
		}
	
	public long getIdutilisateur() {
		return idutilisateur;
	}
	public void setIdutilisateur(long idutilisateur) {
		this.idutilisateur = idutilisateur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
	

}

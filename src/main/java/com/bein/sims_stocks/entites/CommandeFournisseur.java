package com.bein.sims_stocks.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="cmdefournisseur")
public class CommandeFournisseur implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long idcmdefour;
	private String code;
    
    @Temporal(TemporalType.TIMESTAMP)
	private Date datecmdefour;
	
    @ManyToOne
	@JoinColumn(name="idfour")
	private Fournisseur fournisseur;
	
	@OneToMany(mappedBy="commandeFournisseur")
	private List<Lignecmdefournisseur>lignecmdefour;
	
    public CommandeFournisseur() {
		// TODO Auto-generated constructor stub
	}

	public long getIdcmdefour() {
		return idcmdefour;
	}

	public void setIdcmdefour(long idcmdefour) {
		this.idcmdefour = idcmdefour;
	}

	public Date getDatecmdefour() {
		return datecmdefour;
	}

	public void setDatecmdefour(Date datecmdefour) {
		this.datecmdefour = datecmdefour;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<Lignecmdefournisseur> getLignecmdefour() {
		return lignecmdefour;
	}

	public void setLignecmdefour(List<Lignecmdefournisseur> lignecmdefour) {
		this.lignecmdefour = lignecmdefour;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
    
}

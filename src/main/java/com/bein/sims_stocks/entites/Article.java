package com.bein.sims_stocks.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="article")
public class Article implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private long idarticle;
    private String codearticle;
    private String designation;
    private String photo;
    private BigDecimal prixUHT;
    private BigDecimal prixUTT;
    private BigDecimal tauxtva;
    //Constructeur par defaut...
    public Article() {
	
	}
    @ManyToOne
    @JoinColumn(name="idcat")
    private Categorie categorie;

	public long getIdarticle() {
		return idarticle;
	}

	public void setIdarticle(long idarticle) {
		this.idarticle = idarticle;
	}

	public String getCodearticle() {
		return codearticle;
	}

	public void setCodearticle(String codearticle) {
		this.codearticle = codearticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public BigDecimal getPrixUHT() {
		return prixUHT;
	}

	public void setPrixUHT(BigDecimal prixUHT) {
		this.prixUHT = prixUHT;
	}

	public BigDecimal getPrixUTT() {
		return prixUTT;
	}

	public void setPrixUTT(BigDecimal prixUTT) {
		this.prixUTT = prixUTT;
	}

	public BigDecimal getTauxtva() {
		return tauxtva;
	}

	public void setTauxtva(BigDecimal tauxtva) {
		this.tauxtva = tauxtva;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
     
   
}

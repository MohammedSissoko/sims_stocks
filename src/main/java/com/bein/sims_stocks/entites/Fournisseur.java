package com.bein.sims_stocks.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="fournisseur")
public class Fournisseur implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
        private long idfour;
	    private String nom;
	    private String prenom;
	    private String addresse;
	    private String mail;
	    private String photo;
	    @OneToMany(mappedBy="fournisseur")
	    private List<CommandeFournisseur>liscmdefounisseur;
		
	    public Fournisseur() {
			super();
				}

		public long getIdfour() {
			return idfour;
		}

		public void setIdfour(long idfour) {
			this.idfour = idfour;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public String getAddresse() {
			return addresse;
		}

		public void setAddresse(String addresse) {
			this.addresse = addresse;
		}

		public String getMail() {
			return mail;
		}

		public void setMail(String mail) {
			this.mail = mail;
		}

		public String getPhoto() {
			return photo;
		}

		public void setPhoto(String photo) {
			this.photo = photo;
		}

		public List<CommandeFournisseur> getLiscmdefounisseur() {
			return liscmdefounisseur;
		}

		public void setLiscmdefounisseur(List<CommandeFournisseur> liscmdefounisseur) {
			this.liscmdefounisseur = liscmdefounisseur;
		}
	    
        
	
}

package com.bein.sims_stocks.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lignecmdefournisseur")
public class Lignecmdefournisseur implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long lignecmdefour;

	@ManyToOne
	@JoinColumn(name = "idarticle")
	private Article article;

	@ManyToOne
	@JoinColumn(name = "idcmdefour")
	private CommandeFournisseur commandeFournisseur;

	public Lignecmdefournisseur() {
		super();
	}

	public long getLignecmdefour() {
		return lignecmdefour;
	}

	public void setLignecmdefour(long lignecmdefour) {
		this.lignecmdefour = lignecmdefour;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

}

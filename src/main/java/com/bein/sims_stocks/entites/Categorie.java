package com.bein.sims_stocks.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="categorie")
public class Categorie implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long idcat;
	private String code;
	private String designation;
	
	//constructeur
	public Categorie() {
		}
	@OneToMany(mappedBy="categorie")
	private List<Article> listarticle;

	public long getIdcat() {
		return idcat;
	}

	public void setIdcat(long idcat) {
		this.idcat = idcat;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getListarticle() {
		return listarticle;
	}

	public void setListarticle(List<Article> listarticle) {
		this.listarticle = listarticle;
	}
	
	
}

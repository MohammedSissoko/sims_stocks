package com.bein.sims_stocks.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="lignecmdeclient")
public class Lignecmdeclient implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long idlignecmdclient;
	
	@ManyToOne
	@JoinColumn(name="idarticle")
	private Article article;

	@ManyToOne
	@JoinColumn(name="idcmdeclient")
	private CommandeClient commandeClient;
	
	public Lignecmdeclient() {
		}

	public long getIdlignecmdclient() {
		return idlignecmdclient;
	}

	public void setIdlignecmdclient(long idlignecmdclient) {
		this.idlignecmdclient = idlignecmdclient;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}

	
}

package com.bein.sims_stocks.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="vente")
public class Vente implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long idvente;
    
	private String code;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date datevente;
    
    @OneToMany(mappedBy="vente")
    private List<Lignevente> lignevente;

	public long getIdvente() {
		return idvente;
	}

	public void setIdvente(long idvente) {
		this.idvente = idvente;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDatevente() {
		return datevente;
	}

	public void setDatevente(Date datevente) {
		this.datevente = datevente;
	}

	public List<Lignevente> getLignevente() {
		return lignevente;
	}

	public void setLignevente(List<Lignevente> lignevente) {
		this.lignevente = lignevente;
	}
    
   
	
}

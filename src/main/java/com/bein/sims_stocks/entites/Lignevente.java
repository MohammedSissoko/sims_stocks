package com.bein.sims_stocks.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="lignevente")
public class Lignevente implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long idlignevente;
	
	@ManyToOne
	@JoinColumn(name="idarticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idvente")
	private Vente vente;

	public long getIdlignevente() {
		return idlignevente;
	}

	public void setIdlignevente(long idlignevente) {
		this.idlignevente = idlignevente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	

}

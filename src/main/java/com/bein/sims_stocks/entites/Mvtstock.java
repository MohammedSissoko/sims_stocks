package com.bein.sims_stocks.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="mvtstock")
public class Mvtstock implements Serializable
{
	// Constante d entre ou de sortie des stocks
	public static final int ENTREE=1;
	public static final int SROTIE=2;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long idmvtstock;
	
    @Temporal(TemporalType.TIMESTAMP)
	private Date datestock;
    
	private BigDecimal qte;
	private int typemvt;
	@ManyToOne
	@JoinColumn(name="idarticle")
    private Article article;
	
	public Mvtstock() {
		super();
		}

	public long getIdmvtstock() {
		return idmvtstock;
	}

	public void setIdmvtstock(long idmvtstock) {
		this.idmvtstock = idmvtstock;
	}

	public Date getDatestock() {
		return datestock;
	}

	public void setDatestock(Date datestock) {
		this.datestock = datestock;
	}

	public BigDecimal getQte() {
		return qte;
	}

	public void setQte(BigDecimal qte) {
		this.qte = qte;
	}

	public int getTypemvt() {
		return typemvt;
	}

	public void setTypemvt(int typemvt) {
		this.typemvt = typemvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}

package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Article;

public interface IArticleService {
	
	public Article save(Article Entity);

	public Article update(Article Entity);

	public List<Article> selectall();

	public void remove(long id);

	public Article findbyId(long id);

	public List<Article> selectall(String param1, String param2);

	public Article findone(String param, Object objet);

	public Article findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

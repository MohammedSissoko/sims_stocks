package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.ILigneventeDao;
import com.bein.sims_stocks.entites.Lignevente;
import com.bein.sims_stocks.services.ILigneventeService;

@Transactional
public class LigneventeServiceImpl implements ILigneventeService {
    
	private ILigneventeDao daoservice;
	
	public void setDaoservice(ILigneventeDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Lignevente save(Lignevente Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Lignevente update(Lignevente Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Lignevente> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Lignevente findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Lignevente> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Lignevente findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Lignevente findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

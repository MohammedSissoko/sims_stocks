package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.IClientDao;
import com.bein.sims_stocks.entites.Client;
import com.bein.sims_stocks.services.IClientService;

@Transactional
public class ClientServiceImpl implements IClientService {
    
	private IClientDao daoservice;
	
	public void setDaoservice(IClientDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Client save(Client Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Client update(Client Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Client> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Client findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Client> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Client findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Client findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

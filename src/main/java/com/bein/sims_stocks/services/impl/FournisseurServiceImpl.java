package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.IFournisseurDao;
import com.bein.sims_stocks.entites.Fournisseur;
import com.bein.sims_stocks.services.IFournisseurService;

@Transactional
public class FournisseurServiceImpl implements IFournisseurService {
    
	private IFournisseurDao daoservice;
	
	public void setDaoservice(IFournisseurDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Fournisseur save(Fournisseur Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Fournisseur update(Fournisseur Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Fournisseur> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Fournisseur findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Fournisseur> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Fournisseur findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Fournisseur findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

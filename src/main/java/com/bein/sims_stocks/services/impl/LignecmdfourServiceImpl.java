package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.ILignecmdefournisseurDao;
import com.bein.sims_stocks.entites.Lignecmdefournisseur;
import com.bein.sims_stocks.services.ILignecmdfourService;

@Transactional
public class LignecmdfourServiceImpl implements ILignecmdfourService {
    
	private ILignecmdefournisseurDao daoservice;
	
	public void setDaoservice(ILignecmdefournisseurDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Lignecmdefournisseur save(Lignecmdefournisseur Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Lignecmdefournisseur update(Lignecmdefournisseur Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Lignecmdefournisseur> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Lignecmdefournisseur findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Lignecmdefournisseur> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Lignecmdefournisseur findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Lignecmdefournisseur findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

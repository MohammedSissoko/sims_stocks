package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.ICmdeFournisseurDao;
import com.bein.sims_stocks.entites.CommandeFournisseur;
import com.bein.sims_stocks.services.ICmdefournisseurService;

@Transactional
public class CmdefournisseurServiceImpl implements ICmdefournisseurService {
    
	private ICmdeFournisseurDao daoservice;
	
	public void setDaoservice(ICmdeFournisseurDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public CommandeFournisseur save(CommandeFournisseur Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<CommandeFournisseur> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public CommandeFournisseur findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<CommandeFournisseur> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public CommandeFournisseur findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public CommandeFournisseur findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.ILignecmdeclientDao;
import com.bein.sims_stocks.entites.Lignecmdeclient;
import com.bein.sims_stocks.services.ILignecmdclientService;

@Transactional
public class LignecmdclientServiceImpl implements ILignecmdclientService {
    
	private ILignecmdeclientDao daoservice;
	
	public void setDaoservice(ILignecmdeclientDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Lignecmdeclient save(Lignecmdeclient Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Lignecmdeclient update(Lignecmdeclient Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Lignecmdeclient> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Lignecmdeclient findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Lignecmdeclient> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Lignecmdeclient findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Lignecmdeclient findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.IUtilisateurDao;
import com.bein.sims_stocks.entites.Utilisateur;
import com.bein.sims_stocks.services.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService {
    
	private IUtilisateurDao daoservice;
	
	public void setDaoservice(IUtilisateurDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Utilisateur save( Utilisateur Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Utilisateur update( Utilisateur Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List< Utilisateur> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Utilisateur findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Utilisateur> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Utilisateur findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Utilisateur findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

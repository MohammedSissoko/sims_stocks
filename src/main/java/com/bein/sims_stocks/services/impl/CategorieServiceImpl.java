package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.ICategorieDao;
import com.bein.sims_stocks.entites.Categorie;
import com.bein.sims_stocks.services.ICategorieService;

@Transactional
public class CategorieServiceImpl implements ICategorieService {
    
	private ICategorieDao daoservice;
	
	public void setDaoservice(ICategorieDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Categorie save(Categorie Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Categorie update(Categorie Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Categorie> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Categorie findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Categorie> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Categorie findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Categorie findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

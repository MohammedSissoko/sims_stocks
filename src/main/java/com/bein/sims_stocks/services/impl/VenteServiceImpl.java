package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.IVenteDao;
import com.bein.sims_stocks.entites.Vente;
import com.bein.sims_stocks.services.IVenteService;

@Transactional
public class VenteServiceImpl implements IVenteService {
    
	private IVenteDao daoservice;
	
	public void setDaoservice(IVenteDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Vente save(Vente Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Vente update(Vente Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Vente> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Vente findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Vente> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Vente findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Vente findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

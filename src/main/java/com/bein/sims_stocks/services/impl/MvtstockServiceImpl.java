package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.IMvtstockDao;
import com.bein.sims_stocks.entites.Mvtstock;
import com.bein.sims_stocks.services.IMvtstckService;

@Transactional
public class MvtstockServiceImpl implements IMvtstckService {

	private IMvtstockDao daoservice;
	
	public void setDaoservice(IMvtstockDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Mvtstock save(Mvtstock Entity) {
		return daoservice.save(Entity);
		
	}

	@Override
	public Mvtstock update(Mvtstock Entity) {
		
		return daoservice.update(Entity);
	}

	@Override
	public List<Mvtstock> selectall() {
		// TODO Auto-generated method stub
		return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		// TODO Auto-generated method stub
		daoservice.remove(id);
	}

	@Override
	public Mvtstock findbyId(long id) {
		// TODO Auto-generated method stub
		return daoservice.findbyId(id);
	}

	@Override
	public List<Mvtstock> selectall(String param1, String param2) {
		// TODO Auto-generated method stub
		return daoservice.selectall(param1, param2);
	}

	@Override
	public Mvtstock findone(String param, Object objet) {
		// TODO Auto-generated method stub
		return daoservice.findone(param, objet);
	}

	@Override
	public Mvtstock findone(String[] param, Object[] objets) {
		// TODO Auto-generated method stub
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		// TODO Auto-generated method stub
		return daoservice.findcountby(paramname, paramvalue);
	}
   
}

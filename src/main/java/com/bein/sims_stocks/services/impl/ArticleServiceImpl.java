package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.IArticleDao;
import com.bein.sims_stocks.entites.Article;
import com.bein.sims_stocks.services.IArticleService;

@Transactional
public class ArticleServiceImpl implements IArticleService {
    
	private IArticleDao daoservice;
	
	public void setDaoservice(IArticleDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public Article save(Article Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public Article update(Article Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<Article> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public Article findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<Article> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public Article findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public Article findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

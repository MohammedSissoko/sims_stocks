package com.bein.sims_stocks.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.bein.sims_stock.dao.ICmdeclientDao;
import com.bein.sims_stocks.entites.CommandeClient;
import com.bein.sims_stocks.services.ICmdeclientService;

@Transactional
public class CmdeclientServiceImpl implements ICmdeclientService {
    
	private ICmdeclientDao daoservice;
	
	public void setDaoservice(ICmdeclientDao daoservice) {
		this.daoservice = daoservice;
	}

	@Override
	public CommandeClient save(CommandeClient Entity) {
			return daoservice.save(Entity);
	}

	@Override
	public CommandeClient update(CommandeClient Entity) {
		return daoservice.update(Entity);
	}

	@Override
	public List<CommandeClient> selectall() {
			return daoservice.selectall();
	}

	@Override
	public void remove(long id) {
		daoservice.remove(id);	
	}

	@Override
	public CommandeClient findbyId(long id) {
		return daoservice.findbyId(id);
	}

	@Override
	public List<CommandeClient> selectall(String param1, String param2) {
			return daoservice.selectall(param1, param2);
	}

	@Override
	public CommandeClient findone(String param, Object objet) {
			return daoservice.findone(param, objet);
	}

	@Override
	public CommandeClient findone(String[] param, Object[] objets) {
		return daoservice.findone(param, objets);
	}

	@Override
	public int findcountby(String paramname, String paramvalue) {
		return daoservice.findcountby(paramname, paramvalue);
	}

}

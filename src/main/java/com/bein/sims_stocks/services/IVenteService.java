package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Vente;

public interface IVenteService {
	
	public Vente save(Vente Entity);

	public Vente update(Vente Entity);

	public List<Vente> selectall();

	public void remove(long id);

	public Vente findbyId(long id);

	public List<Vente> selectall(String param1, String param2);

	public Vente findone(String param, Object objet);

	public Vente findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

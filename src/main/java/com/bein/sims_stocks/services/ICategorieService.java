package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Categorie;

public interface ICategorieService {
	
	public Categorie save(Categorie Entity);

	public Categorie update(Categorie Entity);

	public List<Categorie> selectall();

	public void remove(long id);

	public Categorie findbyId(long id);

	public List<Categorie> selectall(String param1, String param2);

	public Categorie findone(String param, Object objet);

	public Categorie findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

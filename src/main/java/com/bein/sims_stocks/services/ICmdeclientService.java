package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.CommandeClient;

public interface ICmdeclientService {
	
	public CommandeClient save(CommandeClient Entity);

	public CommandeClient update(CommandeClient  Entity);

	public List<CommandeClient > selectall();

	public void remove(long id);

	public CommandeClient findbyId(long id);

	public List<CommandeClient> selectall(String param1, String param2);

	public CommandeClient findone(String param, Object objet);

	public CommandeClient findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

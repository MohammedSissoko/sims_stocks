package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.CommandeFournisseur;

public interface ICmdefournisseurService {
	
	public CommandeFournisseur save(CommandeFournisseur Entity);

	public CommandeFournisseur update(CommandeFournisseur Entity);

	public List<CommandeFournisseur> selectall();

	public void remove(long id);

	public CommandeFournisseur findbyId(long id);

	public List<CommandeFournisseur> selectall(String param1, String param2);

	public CommandeFournisseur findone(String param, Object objet);

	public CommandeFournisseur findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

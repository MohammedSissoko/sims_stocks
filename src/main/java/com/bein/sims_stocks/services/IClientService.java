package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Client;

public interface IClientService {
	
	public Client save(Client Entity);

	public Client update(Client Entity);

	public List<Client> selectall();

	public void remove(long id);

	public Client findbyId(long id);

	public List<Client> selectall(String param1, String param2);

	public Client findone(String param, Object objet);

	public Client findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Fournisseur;

public interface IFournisseurService {
	
	public Fournisseur save(Fournisseur Entity);

	public Fournisseur update(Fournisseur Entity);

	public List<Fournisseur> selectall();

	public void remove(long id);

	public Fournisseur findbyId(long id);

	public List<Fournisseur> selectall(String param1, String param2);

	public Fournisseur findone(String param, Object objet);

	public Fournisseur findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

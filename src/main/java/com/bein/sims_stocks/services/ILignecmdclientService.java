package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Lignecmdeclient;

public interface ILignecmdclientService {
	
	public Lignecmdeclient save(Lignecmdeclient Entity);

	public Lignecmdeclient update(Lignecmdeclient Entity);

	public List<Lignecmdeclient> selectall();

	public void remove(long id);

	public Lignecmdeclient findbyId(long id);

	public List<Lignecmdeclient> selectall(String param1, String param2);

	public Lignecmdeclient findone(String param, Object objet);

	public Lignecmdeclient findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Lignecmdefournisseur;

public interface ILignecmdfourService {
	
	public Lignecmdefournisseur save(Lignecmdefournisseur Entity);

	public Lignecmdefournisseur update(Lignecmdefournisseur Entity);

	public List<Lignecmdefournisseur> selectall();

	public void remove(long id);

	public Lignecmdefournisseur findbyId(long id);

	public List<Lignecmdefournisseur> selectall(String param1, String param2);

	public Lignecmdefournisseur findone(String param, Object objet);

	public Lignecmdefournisseur findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

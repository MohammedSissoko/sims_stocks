package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Lignevente;

public interface ILigneventeService {
	
	public Lignevente save(Lignevente Entity);

	public Lignevente update(Lignevente Entity);

	public List<Lignevente> selectall();

	public void remove(long id);

	public Lignevente findbyId(long id);

	public List<Lignevente> selectall(String param1, String param2);

	public Lignevente findone(String param, Object objet);

	public Lignevente findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

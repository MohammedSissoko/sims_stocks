package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Utilisateur;


public interface IUtilisateurService {
	
	public Utilisateur save(Utilisateur Entity);

	public Utilisateur update(Utilisateur Entity);

	public List<Utilisateur> selectall();

	public void remove(long id);

	public Utilisateur findbyId(long id);

	public List<Utilisateur> selectall(String param1, String param2);

	public Utilisateur findone(String param, Object objet);

	public Utilisateur findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

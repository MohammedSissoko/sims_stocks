package com.bein.sims_stocks.services;

import java.util.List;

import com.bein.sims_stocks.entites.Mvtstock;

public interface IMvtstckService {
	
	public Mvtstock save(Mvtstock Entity);

	public Mvtstock update(Mvtstock Entity);

	public List<Mvtstock> selectall();

	public void remove(long id);

	public Mvtstock findbyId(long id);

	public List<Mvtstock> selectall(String param1, String param2);

	public Mvtstock findone(String param, Object objet);

	public Mvtstock findone(String[] param, Object[] objets);

	public int findcountby(String paramname, String paramvalue);

}

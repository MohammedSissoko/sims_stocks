package com.bein.sims_stock.dao;

import com.bein.sims_stocks.entites.Client;

public interface IClientDao  extends IGenericDAO<Client> {

}

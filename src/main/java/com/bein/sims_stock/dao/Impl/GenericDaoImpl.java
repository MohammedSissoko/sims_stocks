package com.bein.sims_stock.dao.Impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.bein.sims_stock.dao.IGenericDAO;
@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements IGenericDAO<E> {
    
	@PersistenceContext
	EntityManager em;
	
	private Class<E> type;
	
	public Class<E> getType() {
		return type;
	}
	public GenericDaoImpl() {
	   Type t=getClass().getGenericSuperclass();	
	   ParameterizedType pt=(ParameterizedType) t;
	   type= (Class<E>)pt.getActualTypeArguments()[0];
	}
	@Override
	public E save(E Entity) {
		em.persist(Entity);
		return Entity;
	}

	@Override
	public E update(E Entity) {
		return em.merge(Entity);
	}

	@Override
	public List<E> selectall() {
		//IL s'agit ici du jpql..
		Query query =em.createQuery("select t from " + type.getSimpleName() + "t");
		return query.getResultList();
	}

	@Override
	public void remove(long id) { 
	  E tab=em.getReference(type, id);
	  em.remove(tab);
	}

	@Override
	public E findbyId(long id) {
	   return  em.find(type, id);
	}

	@Override
	public List<E> selectall(String param1, String param2) {
		Query query=em.createQuery("seslect t from " + type.getSimpleName() + "t order by " + param1 + " " + param2 );
		return query.getResultList();
	}

	@Override
	public E findone(String param, Object objet) {
		Query query =em.createQuery("selsect t from" + type.getSimpleName() + "t where " + param + "= :x");
		query.setParameter(param, objet);
		return query.getResultList().size()>0? (E)query.getResultList().get(0):null;
	}

	@Override
	public E findone(String[] param, Object[] objets) {
		if (param.length!= objets.length) {
			return null;
		}
		String querystring="select e from " + type.getSimpleName() + " e where";
		int lon=param.length;
		for(int i=0 ; i<lon; i++) {
			querystring+= "e." + param[i] + "= :x" + i;
			if (i +1< lon) {
				querystring += "and";
			}
		}
		Query query=em.createQuery(querystring);
		for(int i=0; i<objets.length; i++) {
			query.setParameter("x"+1, param[i]);
		}
		return query.getResultList().size()>0? (E)query.getResultList().get(0):null;
	}
	@Override
	public int findcountby(String paramname, String paramvalue) {
		Query query =em.createQuery("selsect t from" + type.getSimpleName() + "t where " + paramname + "= :x");
		query.setParameter(paramname, paramvalue);
		return query.getResultList().size()>0?((Long) query.getSingleResult()).intValue():0;	
	}

}

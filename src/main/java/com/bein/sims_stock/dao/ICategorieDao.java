package com.bein.sims_stock.dao;


import com.bein.sims_stocks.entites.Categorie;

public interface ICategorieDao  extends IGenericDAO<Categorie> {

}

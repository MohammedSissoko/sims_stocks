package com.bein.sims_stock.dao;

import com.bein.sims_stocks.entites.Article;

public interface IArticleDao extends IGenericDAO<Article>{

}

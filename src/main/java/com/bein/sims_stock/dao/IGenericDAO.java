package com.bein.sims_stock.dao;

import java.util.List;

public interface IGenericDAO<E> {
       
	public E save(E Entity);
	public E update(E Entity);
	public List<E> selectall();
	public void remove( long id);
	public E findbyId( long id);
	public List<E> selectall(String param1, String param2);
	public E  findone(String param, Object objet);
	public E findone(String[]param, Object[] objets);
	public int findcountby(String paramname, String paramvalue);
	
}

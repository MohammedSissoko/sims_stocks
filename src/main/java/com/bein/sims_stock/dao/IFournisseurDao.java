package com.bein.sims_stock.dao;

import com.bein.sims_stocks.entites.Fournisseur;

public interface IFournisseurDao  extends IGenericDAO<Fournisseur> {

}
